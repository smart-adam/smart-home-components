import { Module } from "@nestjs/common";
import { GpioModule } from "./modules/gpio/gpio.module";
import { SocketModule } from "./modules/socket/socket.module";

@Module({
  imports: [GpioModule, SocketModule],
})
export class AppModule {}
