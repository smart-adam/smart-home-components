import { Module } from "@nestjs/common";
import { GpioController } from "./controller/gpio.controller";
import { GpioService } from "./service/gpio.service";

@Module({
  controllers: [GpioController],
  providers: [GpioService],
})
export class GpioModule {
  constructor(private readonly gpioService: GpioService) {
    this.gpioService.exportGpios();

    process.on("exit", () => {
      this.gpioService.unexportGpios();
    });
  }
}
