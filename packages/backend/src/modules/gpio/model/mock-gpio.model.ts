import { BinaryValue, Direction, Edge, Options } from "onoff";
import { Gpio } from "./gpio.model";

export class MockGpio implements Gpio {
  label: string;
  groupId: number;
  _gpio: number;

  private value: BinaryValue;
  private activeLowValue: boolean;
  private edgeValue: Edge;
  private directionValue: Direction;

  constructor(
    ...params: [
      label: string,
      gpio: number,
      direction: Direction,
      edge?: Edge,
      options?: Options
    ]
  ) {
    const [label, gpio, direction, edge, options] = params;

    this.label = `[Mock] ${label}`;
    this.groupId = 0;
    this._gpio = gpio;
    this.directionValue = direction;
    this.edgeValue = edge;
    this.activeLowValue = options?.activeLow;
  }

  activeLow(): boolean {
    return this.activeLowValue;
  }
  async read(): Promise<BinaryValue> {
    return this.value;
  }
  readSync(): BinaryValue {
    return this.value;
  }
  async write(value: BinaryValue): Promise<void> {
    this.value = value;
    return;
  }
  writeSync(value: BinaryValue): void {
    this.value = value;
    return;
  }
  setEdge(edge: Edge): void {
    this.edgeValue = edge;
    return;
  }
  edge(): Edge {
    return this.edgeValue;
  }
  watch(): void {
    return;
  }
  setActiveLow(activeLow: boolean): void {
    this.activeLowValue = activeLow;
    return;
  }
  unwatchAll(): void {
    return;
  }
  setDirection(direction: Direction): void {
    this.directionValue = direction;
    return;
  }
  unwatch(): void {
    return;
  }
  unexport(): void {
    return;
  }
  direction(): Direction {
    return this.directionValue;
  }
}
