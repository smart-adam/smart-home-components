import { Direction, Edge, Gpio as BaseGpio, Options } from "onoff";

export class Gpio extends BaseGpio {
  label: string;
  groupId: number;
  _gpio: number;

  constructor(
    label = "Untitled Gpio pin",
    ...gpioParams: [
      gpio: number,
      direction: Direction,
      edge?: Edge,
      options?: Options
    ]
  ) {
    super(...gpioParams);
    this.label = label;
    this.groupId = 0;
  }
}
