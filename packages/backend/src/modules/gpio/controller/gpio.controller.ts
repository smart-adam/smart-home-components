import {
  Controller,
  Param,
  Get,
  Put,
  Body,
  ParseIntPipe,
  ParseBoolPipe,
} from "@nestjs/common";
import { GpioService } from "../service/gpio.service";
import { GpioPin, GpioValue } from "../../../util/types";

@Controller("gpio")
export class GpioController {
  constructor(private readonly gpioService: GpioService) {}

  @Get()
  getGpioList(): GpioPin[] {
    return this.gpioService.getGpioList();
  }

  @Get("all")
  getAllGpioValue(): Promise<GpioValue[]> {
    return this.gpioService.getAllGpioValue();
  }

  @Put("all")
  setAllGpioValue(
    @Body("value", ParseBoolPipe) value: boolean
  ): Promise<GpioValue[]> {
    return this.gpioService.setAllGpioValue(value);
  }

  @Put(":id")
  setGpioValue(
    @Param("id", ParseIntPipe) id: number,
    @Body("value", ParseBoolPipe) value: boolean
  ): Promise<GpioValue> {
    return this.gpioService.setGpioValue(id, value);
  }
}
