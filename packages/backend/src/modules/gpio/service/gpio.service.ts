import { Injectable, Logger, NotFoundException } from "@nestjs/common";
import { BinaryValue } from "onoff";
import { GpioPin, GpioValue } from "../../../util/types";
import { Gpio } from "../model/gpio.model";
import { MockGpio } from "../model/mock-gpio.model";

// TODO: Ez fog majd DB-ből jönni..
import * as GpioConfig from "./gpioConfig.json";

@Injectable()
export class GpioService {
  /**
   * Majd nem itt kell tárolni a gpio állapotokat,
   * hanem redux és ez a service abból fogja kiszedni
   * őket illetve abba tölti be őket. Itt addig van
   * kezelve, amíg meg nincs a "közös" state.
   */
  mockMode = false;
  gpioConfig = GpioConfig as GpioPin[];
  gpios: Gpio[] = [];

  constructor() {
    if (!Gpio.accessible) {
      this.mockMode = true;
      Logger.warn(
        "Gpio is not accessible, operating in MOCK MODE!",
        this.constructor.name
      );
    }
  }

  exportGpios(): void {
    const { gpios } = this;
    this.gpioConfig.forEach((gpioConfigItem) =>
      gpios.push(
        this.mockMode
          ? new MockGpio(
              gpioConfigItem.label,
              gpioConfigItem.pin,
              "high",
              "none",
              {
                activeLow: true,
              }
            )
          : new Gpio(gpioConfigItem.label, gpioConfigItem.pin, "high", "none", {
              activeLow: true,
            })
      )
    );
  }

  unexportGpios(): void {
    this.gpios.forEach((gpio) => {
      gpio.unexport();
    });
  }

  getGpioList(): GpioPin[] {
    return this.gpios.map((gpio) => ({
      label: gpio.label,
      pin: gpio._gpio,
      groupId: gpio.groupId,
    }));
  }

  getAllGpioValue(): Promise<GpioValue[]> {
    return Promise.all(
      this.gpios.map(
        async (gpio): Promise<GpioValue> => ({
          pin: gpio._gpio,
          value: !!(await gpio.read()),
        })
      )
    );
  }

  async setAllGpioValue(value: boolean): Promise<GpioValue[]> {
    await Promise.all(
      this.gpios.map((gpio) => gpio.write(Number(value) as BinaryValue))
    );

    return this.getAllGpioValue();
  }

  async setGpioValue(id: number, value: boolean): Promise<GpioValue> {
    const foundGpio = this.gpios.find((gpio) => gpio._gpio === id);

    if (!foundGpio) {
      throw new NotFoundException();
    }

    await foundGpio.write(Number(value) as BinaryValue);
    return this.getGpioValue(id);
  }

  private async getGpioValue(id: number): Promise<GpioValue> {
    const foundGpio = this.gpios.find((gpio) => gpio._gpio === id);

    return {
      pin: id,
      value: !!(await foundGpio.read()),
    };
  }
}
