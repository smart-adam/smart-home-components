export type GpioPin = {
  label: string;
  pin: number;
  groupId: number;
};

export type GpioValue = {
  pin: number;
  value: boolean;
};
