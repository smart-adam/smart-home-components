import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IGpio } from '../interfaces/gpio.interface';

@Injectable({
  providedIn: 'root'
})
export class NetworkService {
  constructor(private readonly httpClient: HttpClient) { }

  setAllGpioValue(value: boolean): Observable<IGpio[]> {
    return this.httpClient.put<IGpio[]>('http://192.168.0.170:3000/gpio/all', { value: value ? 1 : 0 });
  }

  setGpioValue(id: number, value: boolean): Observable<IGpio> {
    return this.httpClient.put<IGpio>(`http://192.168.0.170:3000/gpio/${id}`, { value: value ? 1 : 0 });
  }

  getGpioList(): Observable<IGpio[]> {
    return this.httpClient.get<IGpio[]>('http://192.168.0.170:3000/gpio');
  }
}
