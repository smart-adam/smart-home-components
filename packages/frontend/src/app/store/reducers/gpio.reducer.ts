import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createReducer,
  createSelector,
  MetaReducer,
  on,
} from '@ngrx/store';
import { environment } from '../../../environments/environment';
import { IState } from '../../interfaces/store.interface';
import * as GpioActions from '../actions/gpio.actions';
import { IGpio } from 'src/app/interfaces/gpio.interface';

// TODO: https://itnext.io/ngrx-best-practices-for-enterprise-angular-applications-6f00bcdf36d7
const appState: IState = {
  gpioList: [],
};

export const reducers: ActionReducerMap<IState> = {
  gpioList: createReducer(
    appState.gpioList,
    on(GpioActions.setAllGpioValue, (state, { payload }) => {
      return payload.reduce((gpioList: IGpio[], gpio: IGpio) => {
        const { pin, value } = gpio;

        gpioList.push({
          pin,
          value,
        });

        return gpioList;
      }, []);
    }),
    on(GpioActions.setGpioValue, (state, { payload }) => {
      return state.reduce((gpioList: IGpio[], gpio: IGpio) => {
        const { pin, value } = gpio;

        if (pin === payload.pin) {
          gpioList.push({
            pin: payload.pin,
            value: payload.value,
          });
        } else {
          gpioList.push({
            pin,
            value,
          });
        }

        return gpioList;
      }, []);
    }),
  ),
};

export const metaReducers: MetaReducer<IState>[] = !environment.production ? [] : [];
