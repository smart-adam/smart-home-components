import { createAction, props } from '@ngrx/store';
import { IGpio } from '../../interfaces/gpio.interface';

export const setAllGpioValue = createAction(
  '[GPIO Module] Update GPIO list',
  props<{
    payload: IGpio[]
  }>(),
);

export const setGpioValue = createAction(
  '[GPIO Module] Update GPIO values',
  props<{
    payload: IGpio;
  }>(),
);
