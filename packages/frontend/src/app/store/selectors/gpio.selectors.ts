import { createSelector } from '@ngrx/store';
import { IState } from '../../interfaces/store.interface';

export const selectGpioList = (state: IState) => state.gpioList;

export const selectIsAllGpioOn = createSelector(
  selectGpioList,
  (gpioList) => gpioList.length === 0 ? false : gpioList.every((gpio) => gpio.value),
);

export const selectIsAllGpioOff = createSelector(
  selectGpioList,
  (gpioList) => gpioList.length === 0 ? true : gpioList.every((gpio) => !gpio.value),
);
