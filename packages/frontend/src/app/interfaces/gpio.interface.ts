export interface IGpio {
  pin: number;
  value: boolean;
}
