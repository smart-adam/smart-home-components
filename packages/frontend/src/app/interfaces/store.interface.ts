import { IGpio } from './gpio.interface';

export interface IState {
  gpioList: IGpio[];
}
