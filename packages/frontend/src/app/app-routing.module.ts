import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RelaySwitchComponent } from './components/relay-switch/relay-switch.component';

const routes: Routes = [
  { path: '', component: RelaySwitchComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
