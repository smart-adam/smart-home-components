export const gpioNames = [
  {
    pin: 3,
    name: 'Esti sárga lámpa',
  },
  {
    pin: 4,
    name: 'Orchidea doboz világítás',
  },
  {
    pin: 5,
    name: 'Orchidea doboz fűtés',
  },
];
