/**
 * ngrx effekt, amit a switchen lő a CLICK event
 *  value
 *  pin
 */

/**
 * Van egy form, amin klikkelek, valamilyen módon visszejön erre egy network válasz. Ezt belefrissítjük a statebe
 */

import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';

import { NetworkService } from '../../services/network.service';

import { Store } from '@ngrx/store';
import { setAllGpioValue, setGpioValue } from '../../store/actions/gpio.actions';
import { selectGpioList, selectIsAllGpioOn, selectIsAllGpioOff } from '../../store/selectors/gpio.selectors';
import { IState } from '../../interfaces/store.interface';
import { IGpio } from '../../interfaces/gpio.interface';

import { Observable, Subject } from 'rxjs';
import { takeLast, takeUntil } from 'rxjs/operators';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';

import { gpioNames } from './config/gpio-names.config';

@Component({
  selector: 'sh-relay-switch',
  templateUrl: './relay-switch.component.html',
  styleUrls: ['./relay-switch.component.scss']
})
export class RelaySwitchComponent implements OnInit, OnDestroy {
  onDestroy$ = new Subject();
  gpioList$: Observable<IGpio[]> = this.store.select(selectGpioList);
  isAllGpioOn$: Observable<boolean> = this.store.select(selectIsAllGpioOn);
  isAllGpioOff$: Observable<boolean> = this.store.select(selectIsAllGpioOff);
  gpioSwitchFormArray = new FormArray([]);

  constructor(
    private readonly networkService: NetworkService,
    private readonly store: Store<IState>,
  ) { }

  ngOnInit(): void {
    this.networkService.getGpioList().pipe(
      takeUntil(this.onDestroy$),
    ).subscribe((gpioList) => {
      this.store.dispatch(
        setAllGpioValue({
          payload: gpioList,
        }),
      );
    });

    this.gpioList$.pipe(
      takeUntil(this.onDestroy$),
    )

    .subscribe((...gpioList) => {
      console.log(gpioList);


      // gpioList.forEach((gpioListItem) => this.gpioSwitchFormArray.push(
      //   new FormControl(gpioListItem.value),
      // ));

      // this.gpioSwitchFormArray.valueChanges.subscribe((formValueArray) => {
      //   this.store.dispatch(
      //     setAllGpioValue({
      //       payload: gpioList,
      //     }),
      //   );
      // });
    });
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
  }

  setAllGpioValue(evt: boolean): void {
    this.networkService.setAllGpioValue(evt).pipe(
      takeUntil(this.onDestroy$),
    ).subscribe((gpioList) => {
      this.store.dispatch(
        setAllGpioValue({
          payload: gpioList,
        }),
      );
    });
  }

  setGpioValue(Gpio: number, evt: MatSlideToggleChange): void {
    this.networkService.setGpioValue(Gpio, evt.checked).pipe(
      takeUntil(this.onDestroy$),
    ).subscribe((gpio) => {
      this.store.dispatch(
        setGpioValue({
          payload: gpio,
        }),
      );
    });
  }

  // NOTE: This won't be hardcoded like this but be this enough for a while..
  getNameForPin(pin: number): string | void {
    const foundName = gpioNames.find((gpioNamesItem) => gpioNamesItem.pin === pin);
    return foundName ? foundName.name : undefined;
  }

  something(evt: MouseEvent): void {
    // evt.preventDefault();
  }
}
