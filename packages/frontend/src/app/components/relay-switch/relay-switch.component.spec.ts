import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RelaySwitchComponent } from './relay-switch.component';

describe('RelaySwitchComponent', () => {
  let component: RelaySwitchComponent;
  let fixture: ComponentFixture<RelaySwitchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RelaySwitchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RelaySwitchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
